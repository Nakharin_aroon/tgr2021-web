<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'UserController@register');
Route::post('login', [LoginController::class, 'login']);
Route::get('open', 'DataController@open');
Route::post("/push", [ItemController::class, 'push']);
Route::post("/search/lime", [ItemController::class, 'searchLime']);
Route::get("/search/lime", [ItemController::class, 'searchLime']);
Route::post("/search/nlime", [ItemController::class, 'searchNotLime']);
Route::post("/search/all", [ItemController::class, 'searchAll']);
Route::post("/post", [ItemController::class, 'getFoundItem']);
Route::post("/photo", [ItemController::class, 'getphoto']);
Route::post("/addUser", [UserController::class, 'addUser']);
Route::post("/editUser", [UserController::class, 'editUser']);
Route::post("/delUser", [UserController::class, 'delUser']);
Route::get("/User", [UserController::class, 'getUser']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});
