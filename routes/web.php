<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController; 
use App\Http\Controllers\ItemController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ItemController::class, 'index'])->name('index');
Route::get('/data', [ItemController::class, 'data'])->name('data');
Route::get('/list/{kind}', [ItemController::class, 'list'])->name('list');
Route::get('/kind', [ItemController::class, 'kind'])->name('kind');
Route::get('/test', [TestController::class, 'test']);
Route::post("push", [ItemController::class, 'push']);
Route::get('/search', [ItemController::class, 'search'])->name('search');
Route::get('/status', [StatusController::class, 'statusCheck']);
Route::get('/getStatus', [StatusController::class, 'getStatus']);
Route::get('/disable', [StatusController::class, 'inActive']);
Route::get('/login', [LoginController::class, 'renderLogin']);
Route::get('/item', [ItemController::class, 'renderItem'])->name("item");
Route::get('/allUser', [UserController::class, 'renderUser'])->name("user");

