<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([[
            	'found' => "Lime",
            	'qty' => 4, 
        ],[
                'found' => "banana",
                'qty' => 1,
        ],[
                'found' => "Papaya",
                'qty' => 1,
        ],[
                'found' => "PingPong",
                'qty' => 1,
        ]]);
    }
}
