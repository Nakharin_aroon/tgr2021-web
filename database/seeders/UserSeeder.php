<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            	'name' => 'Nakharin',
          	'email' => 'Nakharin_aroon@kkumail.com',
            	'password' => Hash::make('password'),
		'fullname' => 'Nakharin Aroonyadej',
        ],[
            	'name' => "Nipitphon",
       		'email' => "Nipitphon@kkumail.com",
         	'password' => Hash::make('password'),
		'fullname'=> "Nipitphon Suchatpong",
        ],[
           	'name' => "Pawaraid",
            	'email' => "Pond.pawaraid@kkumail.com",
            	'password' => Hash::make('password'),
		'fullname'=> "Pawaraid Niyomlao",
        ],[
            	'name' => "Kittipat",
            	'email' => "Kittipat_dd@kkumail.com",
            	'password' => Hash::make('password'),
		'fullname'=> "Kittipat Daengdee",
        ],[
            	'name' => "Chakphet",
            	'email' => 'chakphet_ph@kkumail.com',
            	'password' => Hash::make('password'),
		'fullname'=> "Chakphet Phachanawan",
        ]]);
    }
}
