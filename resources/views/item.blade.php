<!Doctype html>

<head>
	<meta charset="utf-8">
	<title>Tgr30 Server-Tell you percentage of lime</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<link href="https://code.jquery.com/jquery-3.5.1.min.js">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw==" crossorigin="anonymous"></script>
    
	<style>
		.text-center {
			text-align: center;
		}

		.main-container {
			margin: 50px 100px;
		}

        .card{
            margin: 10px auto;
            flex: 1;
            display: flex;
            flex-direction: row;
            padding: 50px;
            width: 1600px;
        }

        .chart-container{
	    display:flex;
	    flex-direction:row;
            flex: 1;
        }

	#graphCanvas{
	    margin: auto;
	}

        #currentPic{
            object-fit: cover;
            object-position: center;
            height: 600px;
            width: 600px;
        }
	</style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Tgr30 Server</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('index')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('item')}}">Item</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('user')}}">user</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('search')}}">Search</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
    <div class="main-container">
	<div class="card">
		<h1>Status : {{$status->status}}</h1>
	</div>
        <div class="card">
            <img id="currentPic" src="{{asset('/')}}storage/{{$picPath->filename}} ">
            <div class="chart-container">
                <canvas id="graphCanvas"></canvas>
            </div>
        </div>
		<div class="card">
            		<table class="table table-bordered table-striped table-hover align-middle">
				<tr class="table-dark text-center">
					<th>Timestamp</th>
					<th>Item</th>
					<th>Size</th>
				</tr>
				@foreach ($items as $item)
				<tr>
					<th>{{$item->created_at}}</th>
					<th>{{$item->found}}</th>
					<th>{{$item->quality}}</th>
				</tr>
				@endforeach
			</table>
        	</div>
	</div>
<script>
function deActivateStatus(){
	fetch("http://188.166.187.246/getStatus")
        .then(res => res.json())
        .then(data => {
                if(data.status === "Active"){
                        let now = new Date();
                        let min = new Date();
                        // console.log(now.getTime()-min.getTime());
                        let update = new Date(data.updated_at);
                        console.log(now.getTime() - update.getTime());
                        if(now.getTime() - update.getTime() > 60000){
                            fetch("http://188.166.187.246/disable")
                        }else{
                            console.log("Active");
                        }
                }
        })
}
setInterval(deActivateStatus, 60000);	
	fetch("http://188.166.187.246/api/search/lime").then(res => res.json())
	.then(data => {
		let labels = [];
        let datas = [];
		let today = new Date();
        today.setHours(0,0,0,0);
        for(let i = 0;i < 24;i++){
			labels.push(today.getFullYear()+"-"+today.getMonth()+"-"+today.getDay()+":"+i);
            datas.push(0);
		}
        console.log(labels);
		data.forEach(item => {
            let itemTime = new Date(item.created_at)
            if(itemTime >= today){
                datas[itemTime.getHours()] += item.qty;
            }
		})
        createChart(labels,datas);
	})
function createChart(label,data){
var ctx = document.getElementById('graphCanvas');
var costChart = new Chart(ctx, {
    type: 'bar',
    // type: 'bar',
    data: {
        labels: label,
        datasets: [{
	    label: 'Number of Lime every 1 Hour',
            data: data,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        },
        responsive: true,
    }
});
}
deActivateStatus();
createChart();
</script>
</body>
