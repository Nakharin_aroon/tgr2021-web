<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Tgr30 Server-Tell you percentage of lime</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous" />
	<style>
		.text-center {
			text-align: center;
		}

		.main-container {
			margin: 50px 100px;
		}

		.card-container {
			display: flex;
		}

		.card-container>*,
		.card>* {
			flex: 1;
			margin: 50px;
			flex-direction: column;
		}

		.lime {
			background-color: rgb(220, 255, 167);
		}

		.n-lime {
			background-color: rgb(255, 167, 167);
		}

		.percent {
			font-size: 100px;
		}

		.nav-item:hover {
			transform: scale(0.9);
			transition: 0.3s;
		}

		.nav-item:active {
			transform: scale(0.8);
			transition: 0.05s;
		}

		.col-10 {
			width: 100%;
		}
	</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">Tgr30 Server</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="{{route('index')}}">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('item')}}">Item</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('user')}}">user</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('search')}}">Search</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="main-container">
		<div class="card-container">
			<div class="card">
				<form class="search-form">
					<h3>Select</h3>
					<select class="form-select" aria-label="Default select example" name="limeOrNot" id="limeOrNot">
						<option selected value="All">All</option>
						<option value="Lime">Lime</option>
						<option value="nlime">not a lime</option>
					</select>
					<h3 for="created_start">Start</h3>
					<div class="form-group row">
						<div class="col-10">
							<input required class="form-control" type="datetime-local" id="created_start" />
						</div>
					</div>
					<h3 for="created_end">to</h3>
					<div class="form-group row">
						<div class="col-10">
							<input required class="form-control" type="datetime-local" id="created_end" />
						</div>
					</div>
					<br />
					<div class="form-group row">
						<button id="submit" type="button" class="btn btn-primary">
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
		<table class="table table-bordered table-striped table-hover align-middle">
			<thead class="table-dark text-center">
				<tr>
					<th>timestamp</th>
					<th>found</th>
					<th>qty</th>
				</tr>
			</thead>
			<tbody id="data">
			</tbody>
		</table>
	</div>
	<script>
		let selectedData = document.getElementById("limeOrNot");
		let dateStart = document.getElementById("created_start");
		let dateEnd = document.getElementById("created_end");
		let submitBtn = document.getElementById("submit");
		let tbody = document.getElementById("data");
		submitBtn.addEventListener("click", (e) => {
			let start = new Date(dateStart.value);
			let end = new Date(dateEnd.value);
			let url = (selectedData.value == "All") ? "all" : (selectedData.value == "Lime") ? "lime" : "nlime";
			let temp = {
				start: dateStart.value,
				end: dateEnd.value
			};
			fetch(`http://188.166.187.246/api/search/${url}`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify(temp)
				})
				.then((res) => {
					return res.json();
				})
				.then((data) => {
					console.log(data);
					for (let item of data) {
						let itemDate = new Date(item.created_at);
						if (itemDate <= end && itemDate >= start) {
							let trow = document.createElement("tr");
							let dateTD = document.createElement("td");
							dateTD.innerText = item.created_at;
							let foundTD = document.createElement("td");
							foundTD.innerText = item.found;
							let qtyTD = document.createElement("td");
							qtyTD.innerText = item.qty;
							trow.appendChild(dateTD);
							trow.appendChild(foundTD);
							trow.appendChild(qtyTD);
							tbody.appendChild(trow);
							console.log(itemDate);
						}
					}
				});
		});
	</script>
</body>