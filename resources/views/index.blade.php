<!Doctype html>

<head>
	<meta charset="utf-8">
	<title>Tgr30 Server-Tell you percentage of lime</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<style>
		.text-center {
			text-align: center;
		}

		.main-container {
			margin: 50px 100px;
		}

		.card-container {
			display: flex;
		}

		.card-container>*,
		.card>* {
			flex: 1;
			margin: 50px;
		}

		.lime {
			background-color: rgb(220, 255, 167);
		}

		.n-lime {
			background-color: rgb(255, 167, 167);
		}

		.percent {
			font-size: 100px;
		}

		.nav-item:hover {
			transform: scale(0.9);
			transition: 0.3s;
		}

		.nav-item:active {
			transform: scale(0.80);
			transition: 0.05s;
		}

		td {
			padding: 15px 30px !important;
			text-align: center;
		}

		.table-container {
			display: flex;
		}

		.table-container>* {
			flex: 1;
			padding: 0 30px;
		}
	</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">Tgr30 Server</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="{{route('index')}}">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('item')}}">Item</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('user')}}">user</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('search')}}">Search</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<?php $limeAmount = 0; ?>
	@forelse ($lime as $limeItem)
	<?php $limeAmount += $limeItem->qty; ?>
	@empty
	@endforelse
	<?php $nlAmount = 0; ?>
	@forelse ($nlime as $nlimeItem)
	<?php $nlAmount += $nlimeItem->qty; ?>
	@empty
	@endforelse
	<div class="main-container">
		<div class="card-container">
			<?php
			$amount = $limeAmount + $nlAmount;
			?>
			<div class="card lime">
				<div class="card-body">
					<p class="percent text-center">{{ceil($limeAmount/$amount*100)}}%</p>
					<h3 class="text-center"> is a percentage of lime</h3>
				</div>
			</div>
			<div class="card n-lime">
				<div class="card-body">
					<p class="percent text-center">{{ceil(($amount-$limeAmount)/$amount*100)}}%</p>
					<h3 class="text-center"> is Not a lime</h3>
				</div>
			</div>
		</div>
		<div class="table-container">
			<div class="lime-table">
				<h1 class="text-center">Lime</h1>
				<table class="table table-bordered table-striped table-hover align-middle">
					<thead class="table-dark text-center">
						<tr>
							<th>timestamp</th>
							<th>found</th>
							<th>qty</th>
						</tr>
					</thead>
					<?php $limeAmount = 0; ?>
					@forelse ($lime as $item)
					<?php $limeAmount += $item->qty; ?>
					<tr>
						<td>{{$item -> created_at}}</td>
						<td>{{$item -> found}}</td>
						<td>{{$item -> qty}}</td>
					</tr>
					@empty
					<tr>
						<td>No items</td>
					</tr>
					@endforelse
				</table>
			</div>
			<div class="nlime-table">
				<h1 class="text-center">Not A Lime</h1>
				<table class="table table-bordered table-striped table-hover align-middle">
					<thead class="table-dark text-center">
						<tr>
							<th>timestamp</th>
							<th>found</th>
							<th>qty</th>
						</tr>
					</thead>
					@forelse ($nlime as $item)
					<tr>
						<td>{{$item -> created_at}}</td>
						<td>{{$item -> found}}</td>
						<td>{{$item -> qty}}</td>
					</tr>
					@empty
					<tr>
						<td>No items</td>
					</tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
</body>