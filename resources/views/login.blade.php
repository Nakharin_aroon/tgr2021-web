<!DOCTYPE html>

<head>
	<meta charset="utf-8" />
	<title>Tgr30 Server-Tell you percentage of lime</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous" />
    <style>
        #submit{
            margin-top: 10px !important;
        }
        .text-center {
			text-align: center;
		}

        body{
            background-color: rgb(226, 226, 226);
            background: url("IMG_7213.JPG");
            background-position: top;
            background-size: initial;
        }

        .modal-blur{
            position: fixed;
            width: 100vw;
            height: 100vh;
            top: 0;
            left: 0;
        }

		.card-container {
            width: 50%;
            display: flex;
            margin: 100px auto;
		}

		.card {
            background-color: rgba(0, 0, 0, 0.089);
            backdrop-filter: blur(100px);
            margin: 0 auto;
            padding: 0 30px;
            border-radius: 10px;
		}

		.card-container>*,
		.card>* {
			flex: 1;
			margin: 50px;
			flex-direction: column;
		}

		.lime {
			background-color: rgb(220, 255, 167);
		}

		.n-lime {
			background-color: rgb(255, 167, 167);
		}

		.percent {
			font-size: 100px;
		}

		.nav-item:hover {
			transform: scale(0.9);
			transition: 0.3s;
		}

		.nav-item:active {
			transform: scale(0.8);
			transition: 0.05s;
		}

		.col-10 {
		}
        .row{
			margin: 20px 0;
        }
    </style>
</head>
<body>
    <div class="modal-blur"></div>
    <div class="card-container">
        <div class="card">
            <form class="search-form">
                <h1 class="text-center">Login</h1>
                <hr>
                <div class="row">
                    <h3 class="col-2 col-form-label">email</h3>
                    <div class="col-10">
                      <input class="form-control" type="text" placeholder="email" id="email">
                    </div>
                </div>
                <div class="row">
                    <h3 for="example-text-input" class="col-2 col-form-label">password</h3>
                    <div class="col-10">
                      <input class="form-control" type="password" id="password" placeholder="password">
                    </div>
                </div>
                <div class="form-group row">
                    <button id="submit" type="button" class="btn btn-primary">
                        Sign in
                    </button>
                </div>
            </form>
        </div>
    </div>
<script>
        let btn = document.getElementById("submit");
        let email = document.getElementById("email");
        let password = document.getElementById("password");
        btn.addEventListener("click", e => {
            fetch("http://188.166.187.246/api/login",{
                method:"POST",
                headers:{
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify({
                    "email":email.value,
                    "password":password.value.toString()
                })
            })
            .then(res => res.text())
            .then(data => {
                if(parseInt(data)){
                    window.location = "./item"
                }
            })
        })
    </script>
</body>
