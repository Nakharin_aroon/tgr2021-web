<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <title>User List</title>

  <style>
    #table-card {
      margin-top: 60px;
    }

    .card-padding {
      width: 80%;
      padding: 30px;
      padding-top: 0;
      margin: auto;
    }

    .right-align {
      text-align: right;
    }

    .rightm {
      margin-right: 20;
    }

    .bg-model {
      position: absolute;
      width: 100vw;
      height: 100vh;
      background-color: rgba(255, 255, 255, 0.4);
      top: 0;
      right: 0;
      backdrop-filter: blur(10px);
      display: flex;
      justify-content: center;
      align-items: center;
      visibility: hidden;
    }

    .model {
      width: 40vw;
      max-width: 800px;
      min-width: 320px;
      padding: 40px;
      z-index: 1000;
    }
  </style>
</head>

<body style="min-height: 100vh">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Tgr30 Server</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('index')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('item')}}">Item</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('user')}}">user</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('search')}}">Search</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="card card-padding" id="table-card">
    <table class="table" id="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Full Name</th>
          <th scope="col">Email</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody id="tbody"></tbody>
    </table>
  </div>
  <div class="bg-model" id="bgModel">
    <div class="model card">
      <form id="form">
        <h1 id="title-form">Edit Profile</h1>
      </form>
    </div>
  </div>

  <script>
    function close() {
      bgModel.style.visibility = "hidden";
    }
    const users = getUser();
    console.log(users);
    const data = ["name", "fullname", "email", "menu"];
    const bgModel = document.querySelector("#bgModel");

    const title = [{
        id: "name",
        t: "Name"
      },
      {
        id: "fullname",
        t: "Full Name"
      },
      {
        id: "email",
        t: "Email"
      },
      {
        id: "password",
        t: "Password"
      },
      {
        id: "submit",
        t: "Submit"
      },
      {
        id: "close",
        t: "Close"
      },
    ];

    title.forEach((ele) => {
      const div = document.createElement("div");
      div.classList.add("form-group");
      if (ele.id == "submit" || ele.id == "close") {
        div.innerHTML = `<button type="button" style="width: -webkit-fill-available"  id="${ele.id}" class="btn ${
            ele.id == "close" ? "btn-danger" : "btn-primary"
          }">${ele.t}</button>`;

        if (ele.id == "close") {
          div.addEventListener("click", close);
        }
      } else
        div.innerHTML = `
        <label for="${ele.id}">${ele.t}</label>
            <input
              type="${ele.id}"
              class="form-control"
              id="${ele.id}"
              placeholder="${ele.t}"

            />`;

      const form = document.querySelector("#form");
      form.appendChild(div);
    });

    function creatRow(users) {
      users.forEach((ele, index) => {
        const row = document.createElement("tr");
        const th = document.createElement("th");
        th.innerHTML = index + 1;
        row.appendChild(th);
        data.forEach((d) => {
          const td = document.createElement("td");

          if (d == "menu") {
            const btEdit = document.createElement("button");
            const btDelete = document.createElement("button");
            btEdit.setAttribute("class", "btn btn-secondary rightm");
            btEdit.setAttribute("id", "bt-edit");
            btDelete.setAttribute("class", "btn btn-danger");
            btEdit.innerHTML = "Edit";
            btEdit.style.marginRight = "16px"
            btDelete.innerHTML = "Delete";
            btDelete.addEventListener("click", () => {
              deleteUser(ele);
            });

            btEdit.addEventListener("click", () => {
              bgModel.style.visibility = "visible";
              data.forEach((t) => {
                if (t == "menu") return;
                const input = document.getElementById(t);
                input.value = ele[t];
              });
              document.querySelector("#title-form").innerHTML = "Edit User";
              document
                .querySelector("#submit")
                .addEventListener("click", () => {
                  const data = getDataForm();
                  console.log(data);
                  editUser(data);
                });
            });
            td.appendChild(btEdit);
            td.appendChild(btDelete);
          } else td.innerHTML = ele[d];

          row.appendChild(td);
        });
        document.querySelector("#tbody").appendChild(row);
      });
    }

    function getDataForm() {
      const val = {};
      title.forEach((ele) => {
        if (
          ele.id == "submit" ||
          ele.id == "conPassword" ||
          ele.id == "close" ||
          ele.id == "Password"
        )
          return;

        const input = document.getElementById(ele.id);
        val[ele.id] = input.value;
      });

      return val;
    }

    function getDataFormAdd() {
      const val = {};
      title.forEach((ele) => {
        if (
          ele.id == "submit" ||
          ele.id == "conPassword" ||
          ele.id == "close"
        )
          return;
        const input = document.getElementById(ele.id);
        val[ele.id] = input.value;
      });

      return val;
    }

    function getUser() {
      return fetch("http://188.166.187.246/api/User")
        .then((response) => response.json())
        .then((data) => creatRow(data))
        .then(() => {
          const btAdd = document.createElement("button");
          btAdd.setAttribute("class", "btn btn-primary rightm");
          btAdd.innerHTML = "Add User";
          btAdd.addEventListener("click", () => {
            bgModel.style.visibility = "visible";
            const btSadd = document.querySelector("#submit");
            console.log(btSadd);
            btSadd.addEventListener("click", () => {
              console.log("Add!!");
              console.log(getDataFormAdd());
              addUser(getDataFormAdd());
            });
          });

          document.querySelector("#table-card").appendChild(btAdd);
        });
    }

    function deleteUser(data) {
      fetch("http://188.166.187.246/api/delUser", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      }).then(() => location.reload())
    }

    function editUser(data) {
      fetch("http://188.166.187.246/api/editUser", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      }).then(() => location.reload())
    }

    function addUser(data) {
      fetch("http://188.166.187.246/api/addUser", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      }).then(() => location.reload())
    }
  </script>
</body>

</html>
