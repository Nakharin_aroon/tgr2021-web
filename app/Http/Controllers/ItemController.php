<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\FormMultipleUpload;
use App\Models\status;
use Illuminate\Support\Arr;
use Phattarachai\LineNotify\Facade\Line;

class ItemController extends Controller
{
    public function index()
    {
        $item = Item::orderby('created_at', 'desc')->get();
        $lime = Item::where('found', 'lime')->get();
        $nlime = Item::whereNotIn('found', ['lime'])->orderby('created_at', 'desc')->get();
        return view('index', ["items" => $item, "lime" => $lime, "nlime" => $nlime]);
        //return response()->json($item);
    }

    public function data()
    {
        $item = Item::orderby('created_at', 'desc')->get();
        return view('data', ["items" => $item]);
    }

    public function kind()
    {
        $item = Item::groupBy("found")->get(['found']);
        //return view('kind', ["items" => Arr::pluck($item,"found")]);
        return response()->json($item);
    }

    public function list(Request $request, $kind)
    {
        $item = Item::where("found", $kind)->orderby("created_at", "desc")->get();
        //return view('list', ["items" => $item]);
        return response()->json($item);
    }
    public function push(Request $request)
    {
	Line::send("Hello");
        return "hello";
    }

    public function search()
    {
        return view('search');
    }

    public function searchLime(Request $request)
    {
        $lime = Item::where('found', 'lime')->get();
        return response()->json($lime);
    }

    public function searchNotLime(Request $request)
    {
        $nlime = Item::whereNotIn('found', ['lime'])->orderby('created_at', 'desc')->get();
        return response()->json($nlime);
    }

    public function searchAll(Request $request)
    {
        $item = Item::orderby('created_at', 'desc')->get();
        return response()->json($item);
    }

    public function getFoundItem(Request $request)
    {
        $input = $request->all();
        return response()->json($input);
    }

    public function getPhoto(Request $request)
    {
        $path = $request->file('image')->store('image', 'public');
	$input = $request->all();
        $uploadModel = new FormMultipleUpload;
        $uploadModel->filename = $path;
        $uploadModel->save();
	$item = Item::create($input);
        if($input['quality'] == "small"){
	
	// ================================================== 
	Line::imageUrl('http://188.166.187.246/storage/'.$path)->send("small lime detected");
	// ===================================================
	}
	return $item.$path;
    }

    public function renderItem()
    {
        $items = Item::orderby('created_at', 'desc')->get();
        $picturePath =  FormMultipleUpload::orderby('created_at', 'desc')->get(['filename'])->first();
        $status = Status::all()->first();
	//return $status;
        return view('item', ["picPath" => $picturePath, "items" => $items, "status" => $status]);
    }
}
