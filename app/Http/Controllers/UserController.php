<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Arr;

class UserController extends Controller
{
	public function getUser(){
		$input = User::orderBy("created_at",'desc')->get();
		return response()->json($input);
	}

    public function addUser(Request $request){
		$input = $request->all();
		return User::create($input);
	}

	public function editUser(Request $request){
		$input = json_decode($request->getContent());
		$user = User::where("email",$input->email)->get()->first();
		foreach($input as $key => $val){
			$user->$key = $val;
		}
		$user->save();
		return $user;
	}

	public function delUser(Request $request){
		$input = json_decode($request->getContent(), true);
		$user = User::where("email",$input['email'])->get()->first();
		$user->delete();
		return $user;
	}

	public function renderUser(){
		return view('user');
	}
}
