<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Arr;

class LoginController extends Controller
{
    public function renderLogin(){
		return view('login');
	}

    public function login(Request $request){
		$input = $request->all();
		$user = User::where("email",$input['email'])->get()->first();
		if($user->password == $input['password']){
			return 1;
		}else {
			return 0;
		}
	}
}
